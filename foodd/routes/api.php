<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::apiResource('products', 'Api\ProductController');
Route::GET('api/categorie/{id}', 'Api\FrontendController@cate');
Route::get('api/pro', 'Api\FrontendController@index');
Route::get('categorie/{id}', 'Api\FrontendController@cate');
Route::get('Bestproduct', 'Api\FrontendController@Bestproduct');
Route::get('Detail/{id}', 'Api\FrontendController@Detail');
