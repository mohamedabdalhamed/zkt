<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// start Route BACKEND
Route::group(['middleware' => ['auth']], function() {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UserController');
    Route::resource('products', 'ProductController');
    Route::resource('categories', 'category');
    Route::get('order','FrontendController@conyent');
    Route::post('orders','FrontController@ss')->name('orders');
// start Route Bills
Route::get('Bils','BillsController@index')->name('Bils');
Route::get('showBils/{id}','BillsController@show')->name('showBils');
Route::delete('archives/{id}','BillsController@destroy')->name('archives');
Route::get('BackBills','BillsController@BackBills')->name('BackBills');
Route::delete('DELETEfor/{id}','BillsController@DELETES')->name('DELETES');
Route::get('BackToIndex/{id}','BillsController@BackToIndex')->name('BackToIndex');

// end Route Bills
Route::get('Expor','UserController@Export')->name('Export');
Route::get('Exportproduct','ProductController@Export')->name('Exportproduct');

});

// start Route FrontendController
Route::get('/', 'FrontendController@index');
Route::get('categorie/{id}', 'FrontendController@cate');
Route::get('Detail/{id}', 'FrontendController@Detail');
Route::get('Bestproduct', 'FrontendController@Bestproduct');
Route::get('mail', 'FrontendController@maila');
Route::get('event', 'FrontendController@events');
Route::get('About', 'FrontendController@About');
Route::post('sreachpage', 'FrontendController@sareac');
Route::post('prices', 'FrontendController@prices');
Route::get('order','FrontendController@conyent');
Route::post('orders','FrontendController@ss')->name('orders');

//end Route   FrontendController

// start Route Cart
Route::get('cart', 'carts@index');
Route::get('add/{id}', 'carts@addcart');
Route::get('remove/{id}','carts@destroy');
Route::PUT('/Cart/update/{id}','carts@update');
// end Route Cart
