@section('sreach')
<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" type="text/css" href="{{asset('sreach/css/normalize.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('sreach/css/demo.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('sreach/css/component.css')}}" />
		<script src="{{asset('sreach/js/modernizr.custom.js')}}"></script>
@endsection
<!-- header -->
	<div class="agileits_header">
		<div class="w3l_offers">
			<a href="{{url('/')}}">Today's special Offers !</a>
		</div>
		<div class="w3l_search">
			<form action="{{url('sreachpage')}}" method="post" id="A">
					{{csrf_field()}}
				<input type="text" class="sreach" name="Product" value="Search a product..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search a product...';}" required="">
		   <select name="filter" class="sreachbar">
		<option value="pro_name">name</option>
		<option value="pro_price">price</option>
		   </select>
			    <input type="submit" name="" id="add" value=" ">
			</form>
		</div>

		<div class="product_list_header" style="margin-top: 10px;">  
			<a href="{{url('cart')}}" class="cart">View your cart</a>
			
		</div>
		<div class="w3l_header_right">
			<ul>
				<li class="dropdown profile_details_drop">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true"></i><span class="caret"></span></a>
					<div class="mega-dropdown-menu">
						<div class="w3ls_vegetables">
							    <ul class="dropdown-menu drp-mnu">
								@guest
					        <li><a href="{{route('login')}}">Login</a></li> 
						    <li><a href="{{route('login')}}">Sign Up</a></li>
					            @else
							<li><a href="{{url('home')}}">profile</a></li> 

								@endguest
							    </ul>
						</div>                  
					</div>	
				</li>
			</ul>
		</div>
		<div class="w3l_header_right1">
		</div>
		<div class="clearfix"> </div>
	</div>
<!-- script-for sticky-nav -->
	<script>
	$(document).ready(function() {
		 var navoffeset=$(".agileits_header").offset().top;
		 $(window).scroll(function(){
			var scrollpos=$(window).scrollTop(); 
			if(scrollpos >=navoffeset){
				$(".agileits_header").addClass("fixed");
			}else{
				$(".agileits_header").removeClass("fixed");
			}
		 });
		 
	});
	</script>
<!-- //script-for sticky-nav -->
	<div class="logo_products">
		<div class="container">
			<div class="w3ls_logo_products_left">
				<h1><a href="{{url('/')}}"><span>FoodsEarth</span> Store</a></h1>
			</div>
			<div class="w3ls_logo_products_left1">
				<ul class="special_items">
					<li><a href="{{url('event')}}">Events</a><i>/</i></li>
					<li><a href="{{url('About')}}">About Us</a><i>/</i></li>
					<li><a href="{{url('Bestproduct')}}">Best Deals</a><i>/</i></li>
					<li><a href="{{url('mail')}}">Contact Us</a></li>
				</ul>
			</div>
			<div class="w3ls_logo_products_left1">
				<ul class="phone_email">
					<li><i class="fa fa-phone" aria-hidden="true"></i>+201229366164</li>
					<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a >mohamedadbhiamed2@gmail.com</a></li>
				</ul>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>


<script type="text/javascript">
      			 var cou=0; 


$(document).on('click','#add',function() {
    var form=$('#A').serialize();
    var $Product=$('.sreach').value;
	
    var url=$('#A').attr('action');
    // alert(form)
   $.ajax({
    url:url,
    dataType:'json',
    data:form,
    type:'post',
    success:function(data){
// $('#A').hide();
//  $(".ale").show();

if(data.length == 0){
	$('.alert').css('display','block');

$('.closebtn').html("Not Found");
}else{

	$('.flexslider').hide();
	$("#sre").css("background-color","#f5f5f5");

var a='';
  a+="<div class='container'  style='background:#f5f5f5;text-align: center;'>";
a+="<h2 style='      color: #000;  margin-top: 22px;'>Sreach Products</h2>"
    a+="<div class='row' >";

           for(var i=0;i<data.length;i++){
	
    a+="<div class='col-md-4' style='margin:10px;text-align: center;'> ";
a+="<a href='{{url('Detail')}}/"+data[i].id+"'>";
a+="<img src='image/"+data[i].image+"' style='max-width: 200px; max-height: 150px'  >";  
a+="</a>";
a+="<h3 class='text-center' style='color: #000;'>";
a+=data[i].pro_name;
a+="</h3>";
a+="<h5 class='text-center' style='    color: #000;'>";
a+=data[i].pro_price;
a+="</h5>";
		

     a+="</div>";

    a+="</div>";
   }
     a+="</div>";
      a+="</div>";

                                           $("#sre").html(a);


}
    }
   });
    // alert(url);    
return false;
});
</script>