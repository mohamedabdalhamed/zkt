@extends('front.layout.default')

@section('title','product')
@section('content')
	<div class="products-breadcrumb">
		<div class="container">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="{{url('/')}}">Home</a><span>|</span></li>
			
				<li>Foods</li>
			</ul>
		</div>
	</div>
<!-- //products-breadcrumb -->
<!-- banner -->

	<div class="banner">

		<div class="w3l_banner_nav_left">
			<nav class="navbar nav_bottom">
			 <!-- Brand and toggle get grouped for better mobile display -->
			  <div class="navbar-header nav_2">
				  <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
			   </div> 
			   <!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
					<ul class="nav navbar-nav nav_1">
						@foreach($category as $na)
						<li><a href="{{url('cate',$na->id)}}">{{$na->name}}</a></li>
	@endforeach
					</ul>
				 </div><!-- /.navbar-collapse -->
			</nav>
		</div>
						<div id="sre"> </div>

		<div class="w3l_banner_nav_right">
			<div class="w3l_banner_nav_right_banner3">
				<h3>Best Deals For New Products<span class="blink_me"></span></h3>
			</div>
			
				<div class="w3ls_w3l_banner_nav_right_grid1">
					<h6></h6>
					<?php $cou=0; ?>

@include('front.element.fillter')
					@foreach($pro as $a)
					<div class="col-md-4 w3ls_w3l_banner_left" style="margin-top: 10px;">
						<div class="hover14 column">
						<div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
							<div class="agile_top_brand_left_grid_pos">
								<img src="images/offer.png" alt=" " class="img-responsive" />
							</div>
							<div class="agile_top_brand_left_grid1">
								<figure>
									<div class="snipcart-item block">
										<div class="snipcart-thumb">
											<a href="{{url('Detail',$a->id)}}"><img src="{{url('image',$a->image)}}" alt=" "  style="max-width: 200px; max-height: 150px"  class="img-responsive" /></a>
											<p>{{$a->pro_name}}</p>
											<h4>{{$a->spl_price}}<span>{{$a->pro_price}}</span></h4>
										</div>
										<div class="snipcart-details">
											
											<form action="{{url('add')}}" method="GET" id="AA">
												<fieldset>
													<input type="hidden" name="pro_id" id="pro_id<?php echo $cou ?>" value="{{$a->id}}" />
												
													<input type="submit" id="addd<?php echo $cou ?>" name="submit" value="Add to cart" class="button" />
												</fieldset>
													
											</form>
										</div>
									</div>
								</figure>
							</div>
						</div>
						</div>
					</div>
									<?php $cou++ ?>

		@Endforeach
					<div class="clearfix"> {{$pro->links()}}</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<script type="text/javascript">
   		<?php $aaa=count($pro);
for($i=0;$i<$aaa;$i++){


		?>   
$(document).on('click','#addd<?php echo $i ?>',function() {

    var form=$('#pro_id<?php echo $i ?>').val();
    // alert(form)
   $.ajax({
    url:"{{url('add')}}/"+form,
    type:'get',
    success:function(data){
$("#addd<?php echo $i ?>").css("background","blue");


 }
   });
    // alert(url);   


return false;


});
<?php
}
?>
</script>
		@endsection
