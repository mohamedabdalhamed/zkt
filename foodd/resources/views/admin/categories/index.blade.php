@extends('admin.layout.app')


@section('content')
<div class="inner-block">

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>categorie</h2>
            </div>
            <div class="pull-right">
                @can('categories-create')
                <a class="btn btn-success" href="{{ route('categories.create') }}"> Create New categories</a>
                @endcan
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th width="280px">Action</th>
        </tr>
	    @foreach ($categories as $categorie)
	    <tr>
	        <td>{{ ++$i }}</td>
	        <td>{{ $categorie->name }}</td>
	        <td>
            <div class="dropup">
  <button class="dropbtn">Dropup</button>
  <div class="dropup-content">
      <form action="{{ route('categories.destroy',$categorie->id) }}" method="POST">
                    @can('categories-edit')
                    <a class="btn btn-primary" href="{{ route('categories.edit',$categorie->id) }}">Edit</a>
                    @endcan

                    @csrf
                    @method('DELETE')
                    @can('categories-delete')
                    <button type="submit" class="btn btn-danger">Delete</button>
                    @endcan
                </form></div>
</div>
    
	        </td>
	    </tr>
	    @endforeach
    </table>


    {!! $categories->links() !!}


</div>
@endsection