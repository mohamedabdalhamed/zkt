
<!DOCTYPE HTML>
<html>
<head>
<title>Shoppy an Admin Panel Category Flat Bootstrap Responsive Website Template | Inbox :: w3layouts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="{{asset('backend/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all">
<!-- Custom Theme files -->
<link href="{{asset('backend/css/style.css')}}" rel="stylesheet" type="text/css" media="all"/>
<!--js-->
<script src="{{asset('backend/js/jquery-2.1.1.min.js')}}"></script> 
<!--icons-css-->
<link href="{{asset('backend/css/font-awesome.css')}}" rel="stylesheet"> 
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
@yield('bills')
<!--//skycons-icons-->
<style type="text/css">
	.dropbtn {
  background-color: #fc8213;
  color: white;
  text-align: center;
  padding: 16px;
  font-size: 16px;
  border: none;

}

/* The container <div> - needed to position the dropup content */
.dropup {
  position: relative;

  text-align: center;
}

/* Dropup content (Hidden by Default) */
.dropup-content {
  display: none;
  position: absolute;
  bottom: 50px;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
      left: 16px;

}

/* Links inside the dropup */
.dropup-content a {

  text-decoration: none;
}

/* Change color of dropup links on hover */
.dropup-content a:hover {background-color: #ddd}

/* Show the dropup menu on hover */
.dropup:hover .dropup-content {
  display: block;
}

/* Change the background color of the dropup button when the dropup content is shown */
.dropup:hover .dropbtn {
  background-color: #2980B9;
}
</style>
</head>
<body>
    @include('admin.elements.header')
    @yield('content')
    @include('admin.elements.sildebar')
    
<!--scrolling js-->
		<script src="{{asset('backend/js/jquery.nicescroll.js')}}"></script>
		<script src="{{asset('backend/js/scripts.js')}}"></script>
		<!--//scrolling js-->
<script src="{{asset('backend/js/bootstrap.js')}}"> </script>
<!-- mother grid end here-->
</body>
</html> 