@extends('admin.layout.app')


@section('content')
<div class="inner-block">

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Product</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('products.update',$product->id) }}" method="POST"  enctype="multipart/form-data"   >
    	@csrf
        @method('PUT')



        <div class="form-group {{$errors->has('pro_name')?'has-error':''}}">
				<label for="pro_name">product Name</label>
				<input type="text" name="pro_name" id="pro_name" value="{{$product->pro_name}}" class="form-control" >
				<span class="text-danger">{{$errors->first('pro_name')}}</span>
			</div>
				 <div class="form-group {{$errors->has('pro_code')?'has-error':''}}">
				<label for="pro_code">code</label>
				<input type="text" name="pro_code"value="{{$product->pro_code}}" id="pro_code"  class="form-control" >
				<span class="text-danger">{{$errors->first('pro_code')}}</span>
			     </div>
				   <div class="form-group {{$errors->has('pro_price')?'has-error':''}}">
				<label for="pro_price">price</label>
				<input type="text" name="pro_price" id="pro_price"value="{{$product->pro_price}}"  class="form-control" >
				<span class="text-danger">{{$errors->first('pro_price')}}</span>
			       </div>

			          <div class="form-group {{$errors->has('stook')?'has-error':''}}">
				<label for="stook">stook</label>
				<input type="text" name="stook" id="stook"  value="{{$product->stook}}" class="form-control" >
				<span class="text-danger">{{$errors->first('stook')}}</span>
			       </div>


        <div class="form-group {{$errors->has('pro_info')?'has-error':''}}">
				<label for="pro_info">Description</label>
				<textarea  name="pro_info" id="pro_info"  class="form-control"  rows="5">{{$product->pro_info}}</textarea> 
				<span class="text-danger">{{$errors->first('pro_info')}}</span>
			        </div>


    <div class="form-group {{$errors->has('categories_id')?'has-error':''}}">
				<label for="categories_id">Catgeroy</label>

				<select name="categories_id" id="categories_id">
					@foreach($categories as  $cats)
					<option value="{{$cats->id}}">{{$cats->name}}</option>
					@endforeach
				</select>

				<span class="text-danger">{{$errors->first('categories_id')}}</span>
			        </div>
 

    <div class="form-group {{$errors->has('offier')?'has-error':''}}">
				<label for="offier">offering</label>

				<select name="offier" id="offier">
			
					<option value="0">no offier</option>
					<option value="1">offier</option>

				</select>

				<span class="text-danger">{{$errors->first('offier')}}</span>
			        </div>
 


			           <div class="form-group {{$errors->has('image')?'has-error':''}}">
				<label for="image">image</label>
				<input type="file" name="image" id="image"  class="form-control" >
                <input type="hidden" name="image1" value="{{$product->image}}" id="image"  class="form-control" >

				<span class="text-danger">{{$errors->first('image')}}</span>
			           </div> 

                         <div class="form-group {{$errors->has('spl_price')?'has-error':''}}">
				<label for="spl_price">sale price</label>
				<input type="text" name="spl_price" id="spl_price" value="{{$product->spl_price}}"  class="form-control" >
				<span class="text-danger">{{$errors->first('spl_price')}}</span>
			             </div>		  
		</div>

		    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		      <button type="submit" class="btn btn-primary">Submit</button>
		    </div>
		</div>


    </form>


</div>
@endsection