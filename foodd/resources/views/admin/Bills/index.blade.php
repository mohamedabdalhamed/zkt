@extends('admin.layout.app')


@section('content')

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

<div class="inner-block">

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2 >Bills</h2>
            </div>
         
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

<?php $count=1; ?>
    <table class="table table-bordered">
        <tr>
            <th>No</th>
	    <th>name user</th>
		<th>price</th>
		<th>phone</th>
        <th>states</th>
            <th width="280px">Action</th>
        </tr>
	    @foreach ($Bills as $Bill)
	    <tr>

       <td><?php echo  $count++; ?></td>

       <td>      
{{$Bill->name}}
</td>
           
                <td>{{$Bill->total_Price}}</td>

<td>{{$Bill->phone}}</td>
<td><!-- {{$Bill->states}} -->
    
    <select class="form-control">

<option  value="{{$Bill->states}}" <?php if($Bill->states == 0){
                        echo "selected";
                    } ?> class="form-control"> order </option>

                    <option  value="{{$Bill->states}}" <?php if($Bill->states == 1){
                        echo "selected";
                    } ?>> in way </option>
                    <option  value="{{$Bill->states}}" <?php if($Bill->states == 2){
                        echo "selected";
                    } ?>> done </option>

    </select>
</td>
<td>  
  
<div class="dropup">
  <button class="dropbtn">Dropup</button>
  <div class="dropup-content">
    <form method="POST"  style="display: inline-block;" action="{{route('DELETES',$Bill->id)}}">
                    
 
                    @csrf
                    @method('DELETE')
                    @can('Bils-delete')
                    <button type="submit" class="btn btn-danger" >DELETE</button>
                    @endcan

                </form>
                <form  style="    display: contents;" method="POST" action="{{route('archives',$Bill->id)}}">
                    @can('Bils-show')
                    <a class="btn btn-info" href="{{route('showBils',$Bill->id)}}">Show</a>
 @endcan
                    @csrf
                    @method('DELETE')

                    @can('Bils-Archives')
                    <button type="submit" class="btn btn-success" >archive</button>
@endcan
                </form>
  </div>
</div>
    
            </td>

</tr>
	    @endforeach
    </table>
{{$Bills->links()}}



</div>
@endsection