<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="images/favicon.png" rel="icon" />
<title>Flight Booking Invoice - Koice</title>
<meta name="author" content="harnishdesign.net">

<!-- Web Fonts
======================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>

<!-- Stylesheet
======================= -->
<link rel="stylesheet" type="text/css" href="{{asset('bills/vendor/bootstrap/css/bootstrap.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('bills/vendor/font-awesome/all.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('bills/css/stylesheet.css')}}"/>
</head>
<body>
<!-- Container -->
<div class="container-fluid invoice-container"> 
  <!-- Header -->
  <header>
    <div class="row align-items-center">
      <div class="col-sm-7 text-center text-sm-left mb-3 mb-sm-0"> <img id="logo" src="images/logo.png" title="Koice" alt="Koice" /> </div>
      <div class="col-sm-5 text-center text-sm-right">
        <h4 class="mb-0">Invoice</h4>
        <p class="mb-0">Invoice Number - 16835</p>
      </div>
    </div>
    <hr>
  </header>
  
  <!-- Main Content -->
  <main id="printdivcontent">
    <div class="row">
      <div class="col-sm-6 text-sm-right order-sm-1"> <strong>Pay To:</strong>
        <address>
                         
When a request has arrived
        </address>
      </div>
      <div class="col-sm-6 order-sm-0"> <strong>order To:</strong>
        <address>
                    @foreach($order as $a)

{{$a->name}}
<br>
{{$a->Address}}

@endforeach
        </address>

      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 mb-3"> <strong>Payment Method:</strong><br>
        <span>When a request has arrived</span> </div>
      <div class="col-sm-6 mb-3 text-sm-right"> <strong>Booking Date:                  </strong><br>
        <span>@foreach($order as $a)
{{$a->created_at}}
@endforeach</span> </div>
    </div>
    <div class="card">
      <div class="card-header"> <span class="font-weight-600 text-4">Booking Summary</span> </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <td class="col-6 border-top-0"><strong>Product name</strong></td>
                <td class="col-2 text-center border-top-0"><strong>Qty</strong></td>
                <td class="col-2 text-center border-top-0"><strong>Price</strong></td>
              </tr>
            </thead>
            <tbody>

             @foreach($order_Details as $a)
              <tr>


                <td><span class="text-3"><span class="font-weight-500">{{$a->name}}</span></td>
                <td class="text-center">{{$a->qty}}</td>
                <td class="text-center">{{$a->price}}</td>
              </tr>

@endforeach

          
                <td colspan="2" class="bg-light-2 text-right"><strong>Total</strong></td>
                <td colspan="2" class="bg-light-2 text-right">
                  @foreach($order as $a)
{{$a->total_Price}}


@endforeach</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <br>
<!--     <div class="table-responsive d-print-none">
      <table class="table table-bordered">
        <thead>
          <tr>
            <td class="text-center"><strong>Transaction Date</strong></td>
            <td class="text-center"><strong>Gateway</strong></td>
            <td class="text-center"><strong>Transaction ID</strong></td>
            <td class="text-center"><strong>Amount</strong></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-center">07/11/2019</td>
            <td class="text-center">Credit Card</td>
            <td class="text-center">3912912704</td>
            <td class="text-center">$1583.20 USD</td>
          </tr>
        </tbody>
      </table>
    </div> -->
  </main>
  <!-- Footer -->
  <footer class="text-center">
    <p class="text-1"><strong>NOTE :</strong> This is computer generated receipt and does not require physical signature.</p>
    <div class="btn-group btn-group-sm d-print-none"> <a onclick="PrintDiv()" class="btn btn-light border text-black-50 shadow-none" ><i class="fa fa-print"></i> Print</a> <a href="" class="btn btn-light border text-black-50 shadow-none"><i class="fa fa-download"></i> Download</a> </div>
  </footer>
</div>
<!-- Back to My Account Link -->
<p class="text-center d-print-none"><a href="#">&laquo; Back to My Account</a></p>

</body>
  <script type="text/javascript">  
        function PrintDiv() {  
            var divContents = document.getElementById("printdivcontent").innerHTML;  
            var printWindow = window.open('', '', 'height=1000,width=1000');  
            printWindow.document.write('<html><head><title>Print bills Content</title>');
     
            
            printWindow.document.write("<link rel='stylesheet' type='text/css' href='{{asset('bills/vendor/bootstrap/css/bootstrap.min.css')}}'/><link rel='stylesheet'type='text/css' href='{{asset('bills/css/stylesheet.css')}}'/><link rel='stylesheet' type='text/css' href='{{asset('bills/vendor/font-awesome/all.min.css')}}'/></head><body >");  
            printWindow.document.write(divContents);  
            printWindow.document.write('</body></html>');  
            printWindow.document.close();  
            printWindow.print();  
        }  
    </script> 
</html>