@extends('admin.layout.app')


@section('content')

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

<div class="inner-block">

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Archives</h2>
            </div>
         
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

<?php $count=1; ?>
    <table class="table table-bordered">
        <tr>
            <th>No</th>
	    <th>name user</th>
		<th>price</th>
		<th>phone</th>
        <th>states</th>
        <th>Control</th>
        </tr>
	    @foreach ($Bills as $Bill)
	    <tr>

       <td><?php echo  $count++; ?></td>

       <td>      
{{$Bill->name}}
</td>
           
                <td>{{$Bill->total_Price}}</td>

<td>{{$Bill->phone}}</td>
<td>{{$Bill->states}}</td>
<td>
   <form method="POST" action="{{route('DELETES',$Bill->id)}}">
                    
                    @csrf
                    @method('DELETE')
                    @can('Bils-delete')
                    <button type="submit" class="btn btn-danger" >DELETE</button>
                    @endcan

                </form>
                @can('Bils-Archives')
                 <a href="{{route('BackToIndex',$Bill->id)}}" class="btn"style="background-color:#fc8213;color: #FFF;" >Remove Archives</a>
                 @endcan
</td>
            
</tr>
	    @endforeach
    </table>
 


</div>
@endsection