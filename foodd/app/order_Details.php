<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order_Details extends Model
{

	    protected $fillable = [
        'name', 'qty', 'price',
    ];

     public function order()
    {
        return $this->belongsTo('App\order');
    }
  
    }
