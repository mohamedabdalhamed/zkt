<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\products;
class Carts extends Controller
{
    public function index()
    {
 $a=  Cart::content();

        return view('front.pages.cart',compact('a'));	
    }
    public function addcart($id)
    {
     $products=products::find($id);	
    // $aa=Cart::add($id,$products->pro_name,1,,);
    //    
    $aa=Cart::add($id,$products->pro_name, 1,$products->pro_price,$products->stook,['img'=>$products->image]);

     return response($aa);	
    }   
 
    public function destroy($id){
 

        Cart::remove($id);
                return redirect('cart');
    
    }
    public function update(Request $request,$id){
    	$qty=$request->qty;
    	$proID=$request->proID;
    	$product=products::findOrFail($proID);
    	$stook=$product->stook;
    	if($qty<$stook){
    		Cart::update($id,$request->qty);
            return back()->with('status','your update');

    	}else{
            return  back()->with('error','sorry');
	
    	}

} 
}
