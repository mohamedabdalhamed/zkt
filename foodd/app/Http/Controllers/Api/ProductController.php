<?php
    
namespace App\Http\Controllers\Api;
    
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;;
use App\products;
use App\categories;
use App\Http\Resources\product;
class ProductController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         // $this->middleware('permission:product-list|product-create|product-edit|product-delete', ['only' => ['index','show']]);
         // $this->middleware('permission:product-create', ['only' => ['create','store']]);
         // $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
         // $this->middleware('permission:product-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = products::latest()->paginate(5);
       return product::collection($products);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
 
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $this->validate($request,[
            'pro_name'=>'required',
            'pro_code'=>'required',
            'pro_price'=>'required',
            'pro_info'=>'required',
            'spl_price'=>'required',
            
                    ]);
                    $pro=new products();
                        $pro->pro_name=$request->input('pro_name');        
                        $pro->pro_code=$request->input('pro_code');
                        $pro->pro_price=$request->input('pro_price');
                        $pro->pro_info=$request->input('pro_info');
                        $pro->stook=$request->input('stook');
                        $pro->categories_id=$request->input('categories_id');
                        $pro->spl_price=$request->input('spl_price');
                        $pro->offier=$request->input('offier');
                if($request->hasFile('image')){
                              $img=$request->image;
                              $filename=time() ."_".$img->getClientOriginalName();
                              $localtion=public_path('image/'.$filename);
                              //$img->move($localtion);
                              image::make($img)->resize(1000,1000)->save($localtion);
                 
                                              }
            
                              $pro->image=$filename; 
            
                            $pro->save();
    return new product($pro);

    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(products $product)
    {
                $product=product::all();

//     try {
//     }catch(\Exception $e){
// return  response()->Json();
//     }
  return product::collection($product);
        // return view('products.show',compact('product'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(products $product)
    {
$categories=categories::find($product);
        return view('admin.products.edit',compact('product','categories'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     *
     *  @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {       
        $this->validate($request,[
            'pro_name'=>'required',
            'pro_code'=>'required',
            'pro_price'=>'required',
            'pro_info'=>'required',
            'spl_price'=>'required',
            
                    ]);
                    $pro=products::find($id);
                        $pro->pro_name=$request->input('pro_name');        
                        $pro->pro_code=$request->input('pro_code');
                        $pro->pro_price=$request->input('pro_price');
                        $pro->pro_info=$request->input('pro_info');
                        $pro->stook=$request->input('stook');
                        $pro->categories_id=$request->input('categories_id');
                        $pro->spl_price=$request->input('spl_price');
                        $pro->offier=$request->input('offier');
                        if($request->input('image') =='' ) {
                            $pro->image=$request->input('image1'); 
        
                           }   
                        if($request->hasFile('image')){
                              $img=$request->image;
                              $filename=time() ."_".$img->getClientOriginalName();
                              $localtion=public_path('image/'.$filename);
                              //$img->move($localtion);
                              image::make($img)->resize(1000,1000)->save($localtion);
                 
                              $pro->image=$filename; 

                            }
            
            
                            $pro->save();

        return redirect()->route('products.index')
                        ->with('success','Product updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(products $product)
    {
        $product->delete();
    
        return redirect()->route('products.index')
                        ->with('success','Product deleted successfully');
    }
    
 public function Export(){
        return Excel::download(new productsExport, 'products.xlsx');
     }
}