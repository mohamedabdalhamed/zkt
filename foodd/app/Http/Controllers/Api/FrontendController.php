<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\categories;
use App\products;
use App\rating;
use App\event;
use App\User;
use App\order;
use App\Address;
use App\order_Details;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Frontend;

use App\Http\Controllers\Controller;

class FrontendController extends Controller
{

    public function index(){
        $category=categories::all();

        $off=products::all();
    	return   Frontend::collection($off);
    }
    
public function cate($id){

    $category=categories::all();

$cate=categories::all()->where('id',$id);
$pro=products::where('categories_id',$id)->paginate(9);

return   Frontend::collection($pro,$category);
} 

public function Bestproduct(){
    $category=categories::all();
  $pro=products::OrderBy('created_at','ASC')->paginate(9);
return  Frontend::collection($pro,$category);
}

public function Detail($id){

    //       $category=category::all();
        $category=categories::all();
  
    $pro=products::all()->where('id',$id);
    // $m=rating::all()->where('product_id',$id)->sum('ratin');
    // $a=rating::where('product_id',$id)->count();
    // $s=$m/$a;
    $s=5;
    // return view('front.pages.details',compact('pro','s','category'));
        return   Frontend::collection($category,$products);

  } 


  public 	function maila(){
    $category=categories::all();
    return   Frontend::collection($category);

}


public function events(){
$event=event::all();
  $category=categories::all();
return view('front.pages.events',compact('event','category'));
}

public function About(){
$category=categories::all();

return view('front.pages.about',compact('category'));

}

public 	function sareac(Request $request){
    $search=$request->Product;
    $filter=$request->filter;
    
    
      $users = products::where($filter,'like','%'.$search.'%')->get();
    
        return response($users); 
        }
        

        public 	function prices(Request $request){
          $min=$request->price_min;
          $max=$request->price_max;
            $users = products::whereBetween('pro_price',[$min,$max])->get();
          
              return response($users); 
              }
                       public   function conyent(){


              $user=Auth::User();
              $Address=Address::all()->where('user_id',$user->id);
              $total=Cart::total();
              $a=Cart::Content();
              
                       return view('front.pages.order',compact('total','user','Address','a'));  
                        }   
                       
public function ss(Request $request)
{

$order=new order();
$order->name=$request->name;
$order->total_Price=$request->total;
$order->phone=$request->phone;
$order->Address=$request->Address;
$order->user_id=Auth::user()->id;
$order->save();

//   $name =  $request->name;

//   $qty =  $request->qty;

  // $total =  $request->total;
  $rowId =  $request->rowId;
$name=$request->namep;
   $count = count($name);
// $phone=$request->phone;
for($i = 0; $i < $count; $i++){
  $objModel = new order_Details();
  $objModel->order_id=$order->id;
  $objModel->name = $request->namep[$i];
  $objModel->qty =  $request->qty[$i];
  $objModel->price =  $request->totalL[$i];
  $objModel->save();

  Cart::remove($rowId[$i]);
     
  }
return back();
     
}}
