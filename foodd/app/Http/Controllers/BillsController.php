<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order;
use App\User;
use App\order_Details;
use Illuminate\Support\Facades\Auth;


class BillsController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:Bils-list', ['only' => ['index']]);
         $this->middleware('permission:Bils-show', ['only' => ['show']]);
         $this->middleware('permission:Bils-delete', ['only' => ['DELETES','destroy']]);
         $this->middleware('permission:Bils-Archives', ['only' => ['BackToIndex']]);

    }
    public function index()
    {
 
    

$Bills=order::orderBy('id','ASC')->paginate(5);
return view('admin.Bills.index',compact('Bills'));

    }
    public function show($id){
$order=order::all()->where('id',$id);
$order_Details=order_Details::all()->where('order_id',$id);

return view('admin.Bills.Bils',compact('order','order_Details'));

}       
public function destroy($id){
$Bils=order::find($id);
    $Bils->delete();
    
        return redirect()->route('Bils')
                        ->with('success','Bills Archives successfully');
}

                                            public function BackBills()
                                            {
                                                 $Bills = order::withTrashed()->get();
                                            return view('admin.Bills.archives',compact('Bills'));
                                             }
  public function DELETES($id)
                {

                $Bils=order::find($id);
                $Bils->forceDelete();

                return redirect()->route('Bils')
                        ->with('success','Bills deleted successfully');
                 }
                 

        public function BackToIndex($id)
                 {
$id=order::withTrashed()
    ->where('id',$id)
    ->restore();
return redirect()->route('Bils')
                        ->with('success','Bills BackBills successfully');
                 }
}
