<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class order extends Model
{
    use SoftDeletes;

	    protected $fillable = [
        'name', 'total_Price', 'phone','Address',
    ];

        protected $dates = ['deleted_at'];

        public function order_Details()
    {
        return $this->hasOne('App\order_Details');
    }

}
