<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
	protected $fillable = ['Address', 'Address1','land','lang','user_id'];

      public function user()
{
        return $this->belongsTo('App\User');
}

}
