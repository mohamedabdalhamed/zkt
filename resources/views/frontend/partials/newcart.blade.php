@php
if(auth()->user() != null) {
    $user_id = Auth::user()->id;
    $cart = \App\Cart::where('user_id', $user_id)->get();
} else {
    $temp_user_id = Session()->get('temp_user_id');
    if($temp_user_id) {
        $cart = \App\Cart::where('temp_user_id', $temp_user_id)->get();
    }
}

@endphp

  <a href="javascript:void(0)" class="indicator__button" data-toggle="dropdown" data-display="static">
                                            <span class="indicator__area">
                                                <svg width="20px" height="20px">
                                                    <use xlink:href="{{static_asset('frontend/images/sprite.svg#cart-20')}}"></use>
                                                </svg>

                                                <span class="indicator__value">                                                        @if(isset($cart) && count($cart) > 0)
{{count($cart)}}                                        @else
                                                        0
        @endif
                         </span>
                                            </span>
                                        </a>
    <div class="indicator__dropdown">
 <div class="dropcart dropcart--style--dropdown">
                                                <div class="dropcart__body">
                                                      @if(isset($cart) && count($cart) > 0)
                                                 <div class="dropcart__products-list">
                                        @php
                $total = 0;
            @endphp
            @foreach($cart as $key => $cartItem)
                @php
                    $product = \App\Product::find($cartItem['product_id']);
                    $total = $total + $cartItem['price'] * $cartItem['quantity'];
                @endphp
                @if ($product != null)
          
                                                        <div class="dropcart__product">
                                                            <div class="product-image dropcart__product-image">
                                                                <a href="{{ route('product', $product->slug) }}" class="product-image__body">
                                                                    <img class="product-image__img"    src="{{ uploaded_asset($product->thumbnail_img) }}"
                                    data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                                    alt="{{  $product->getTranslation('name')  }}">
                                                                </a>
                                                            </div>
                                                            <div class="dropcart__product-info">
                                                                <div class="dropcart__product-name"><a href="{{ route('product', $product->slug) }}">                                            {{  $product->getTranslation('name')  }}
</a></div>
                                                                <ul class="dropcart__product-options">
                                                                    <li>Color: Yellow</li>
                                                                    <li>Material: Aluminium</li>
                                                                </ul>
                                                                <div class="dropcart__product-meta">
                                                                    <span class="dropcart__product-quantity">{{ $cartItem['quantity'] }}</span> ×
                                                                    <span class="dropcart__product-price">{{single_price($cartItem['price']) }}</span>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="dropcart__product-remove btn btn-light btn-sm btn-svg-icon">
                                                                <svg width="10px" height="10px">
                                                                    <use xlink:href="{{static_asset('frontend/images/sprite.svg#cross-10')}}'"></use>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                                        @endif
                                                                                    @endforeach

                                                                                               <div class="dropcart__totals">
                                                        <table>
                                                       <!--      <tr>
                                                                <th>Subtotal</th>
                                                                <td>{{translate('Subtotal')}}</td>
                                                            </tr> -->
                                                          <!--   <tr>
                                                                <th>Shipping</th>
                                                                <td>$25.00</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Tax</th>
                                                                <td>$0.00</td>
                                                            </tr> -->
                                                            <tr>
                                                                <th>{{translate('Subtotal')}}</th>
                                                                <td>{{ single_price($total) }}</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="dropcart__buttons">
                
                                                        <a class="btn btn-secondary"  href="{{ route('cart') }}" >                
                                                                {{translate('View cart')}}</a>
                                                                    @if (Auth::check())
    <a class="btn btn-primary" href="{{ route('checkout.shipping_info') }}">                        {{translate('Checkout')}}
</a>
                    @else 
    <a class="btn btn-primary" href="Login.html">Login </a>

                    @endif
                                                    </div>
                                                </div>

                                                @else
<div class="text-center p-3">
            <i class="las la-frown la-3x opacity-60 mb-3"></i>
            <h3 class="h6 fw-700">{{translate('Your Cart is empty')}}</h3>
        </div>
                                                @endif
                                            </div>
                                            <!-- .dropcart / end -->
                                        </div>
                                    </div>
<!-- 
   
<div class="dropdown-menu dropdown-menu-right dropdown-menu-lg p-0 stop-propagation">
    
    @if(isset($cart) && count($cart) > 0)
        <div class="p-3 fs-15 fw-600 p-3 border-bottom">
            {{translate('Cart Items')}}
        </div>
        <ul class="h-250px overflow-auto c-scrollbar-light list-group list-group-flush">
            @php
                $total = 0;
            @endphp
            @foreach($cart as $key => $cartItem)
                @php
                    $product = \App\Product::find($cartItem['product_id']);
                    $total = $total + $cartItem['price'] * $cartItem['quantity'];
                @endphp
                @if ($product != null)
                    <li class="list-group-item">
                        <span class="d-flex align-items-center">
                            <a href="{{ route('product', $product->slug) }}" class="text-reset d-flex align-items-center flex-grow-1">
                                <img
                                    src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                                    class="img-fit lazyload size-60px rounded"
                                    alt="{{  $product->getTranslation('name')  }}"
                                >
                                <span class="minw-0 pl-2 flex-grow-1">
                                    <span class="fw-600 mb-1 text-truncate-2">
                                            {{  $product->getTranslation('name')  }}
                                    </span>
                                    <span class="">{{ $cartItem['quantity'] }}x</span>
                                    <span class="">{{ single_price($cartItem['price']) }}</span>
                                </span>
                            </a> -->
                       <!--      <span class="">
                                <button onclick="removeFromCart({{ $cartItem['id'] }})" class="btn btn-sm btn-icon stop-propagation">
                                    <i class="la la-close"></i>
                                </button>
                            </span> -->
    <!--                     </span>
                    </li>
                @endif
            @endforeach
        </ul>
        <div class="px-3 py-2 fs-15 border-top d-flex justify-content-between">
            <span class="opacity-60">{{translate('Subtotal')}}</span>
            <span class="fw-600">{{ single_price($total) }}</span>
        </div>
        <div class="px-3 py-2 text-center border-top">
            <ul class="list-inline mb-0">
                <li class="list-inline-item">
                    <a href="{{ route('cart') }}" class="btn btn-soft-primary btn-sm">
                        {{translate('View cart')}}
                    </a>
                </li>
                @if (Auth::check())
                <li class="list-inline-item">
                    <a href="{{ route('checkout.shipping_info') }}" class="btn btn-primary btn-sm">
                        {{translate('Checkout')}}
                    </a>
                </li>
                @endif
            </ul>
        </div> -->
  <!--   @else
        <div class="text-center p-3">
            <i class="las la-frown la-3x opacity-60 mb-3"></i>
            <h3 class="h6 fw-700">{{translate('Your Cart is empty')}}</h3>
        </div>
    @endif -->
    
<!-- </div>
 -->