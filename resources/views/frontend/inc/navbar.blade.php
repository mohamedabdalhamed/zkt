    <div class="site">
        <!-- mobile site__header -->
        <header class="site__header d-lg-none">
            <!-- data-sticky-mode - one of [pullToShow, alwaysOnTop] -->
            <div class="mobile-header mobile-header--sticky" data-sticky-mode="pullToShow">
                <div class="mobile-header__panel">
                    <div class="container">
                        <div class="mobile-header__body">
                            <button class="mobile-header__menu-button">
                                <svg width="18px" height="14px">
                                    <use xlink:href="frontend/images/sprite.svg#menu-18x14"></use>
                                </svg>
                            </button>
                            <a class="mobile-header__logo" href="index.html">
                                <!-- mobile-logo -->
                            <img src="{{static_asset('frontend/images/zk-logo/logo H_350mm(sticker)ss.png')}}" width="100">
                                <!-- mobile-logo / end -->
                            
                            </a>
                            <div class="search search--location--mobile-header mobile-header__search">
                                <div class="search__body">
                                    <form class="stop-propagation search__form" action="{{ route('search') }}" method="GET" >
                                        <input type="text" class="search__input"name="q" placeholder="{{translate('I am shopping for...')}}" autocomplete="off"
                                         id="search"  >
                                        <button class="search__button search__button--type--submit" type="submit">
                                            <svg width="20px" height="20px">
                                                <use xlink:href="{{static_asset('frontend/images/sprite.svg#search-20')}}"></use>
                                            </svg>
                                        </button>
                                        <button class="search__button search__button--type--close" type="button">
                                            <svg width="20px" height="20px">
                                                <use xlink:href="{{static_asset('frontend/images/sprite.svg#cross-20')}}"></use>
                                            </svg>
                                        </button>
                                        <div class="search__border"></div>
                                    </form>
                                       <div class="typed-search-box stop-propagation document-click-d-none d-none bg-white rounded shadow-lg position-absolute left-0 top-100 w-100" style="min-height: 200px;color: #000">
                            <div class="search-preloader absolute-top-center">
                                <div class="dot-loader"><div></div><div></div><div></div></div>
                            </div>
                            <div class="search-nothing d-none p-3 text-center fs-16">

                            </div>
                            <div class="searchcontent" class="text-left">

                            </div>
                        </div>

                                    <div class="search__suggestions suggestions suggestions--location--mobile-header"></div>
                                </div>
                            </div>
                            <div class="mobile-header__indicators">
                                <div class="indicator indicator--mobile-search indicator--mobile d-md-none">
                                    <button class="indicator__button">
                                        <span class="indicator__area">
                                            <svg width="20px" height="20px">
                                                <use xlink:href="{{static_asset('frontend/images/sprite.svg#search-20')}}"></use>
                                            </svg>
                                        </span>
                                    </button>
                                </div>
                                <div class="indicator indicator--mobile d-sm-flex d-none">
                                    <a href="" class="indicator__button">
                                        <span class="indicator__area">
                                            <svg width="20px" height="20px">
                                                <use xlink:href="{{static_asset('frontend/images/sprite.svg#heart-20')}}"></use>
                                            </svg>
                                            <span class="indicator__value">0</span>
                                        </span>
                                    </a>
                                </div>
                                <div class="indicator indicator--mobile">
                                    <a href="" class="indicator__button">
                                        <span class="indicator__area">
                                            <svg width="20px" height="20px">
                                                <use xlink:href="{{static_asset('frontend/images/sprite.svg#cart-20')}}"></use>
                                            </svg>
                                            <span class="indicator__value">3</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- mobile site__header / end -->
        <!-- desktop site__header -->
        <header class="site__header d-lg-block d-none">
            <div class="site-header">
                <!-- .topbar -->
                <div class="site-header__topbar topbar">
                    <div class="topbar__container container">
                        <div class="topbar__row">
                            <div class="topbar__item topbar__item--link">
                                <a class="topbar-link" href="">About Us</a>
                            </div>
                            <div class="topbar__item topbar__item--link">
                                <a class="topbar-link" href="">Contacts</a>
                            </div>
                            <div class="topbar__item topbar__item--link">
                                <a class="topbar-link" href="">Blog</a>
                            </div>
                            <div class="topbar__spring"></div>
                            <div class="topbar__item">
                                <div class="topbar-dropdown">
                                    <button class="topbar-dropdown__btn" type="button">
                                        My Account
                                        <svg width="7px" height="5px">
                                            <use xlink:href="{{static_asset('frontend/images/sprite.svg#arrow-rounded-down-7x5')}}"></use>
                                        </svg>
                                    </button>
                                    <div class="topbar-dropdown__body">
                                        <!-- .menu -->
                                        <div class="menu menu--layout--topbar ">
                                            <div class="menu__submenus-container"></div>
                                            <ul class="menu__list">
                                                <li class="menu__item">
                                                    <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                    <div class="menu__item-submenu-offset"></div>
                                                    <a class="menu__item-link" href="">
                                                        Dashboard
                                                    </a>
                                                </li>
                                                <li class="menu__item">
                                                    <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                    <div class="menu__item-submenu-offset"></div>
                                                    <a class="menu__item-link" href="">
                                                        Edit Profile
                                                    </a>
                                                </li>
                                                <li class="menu__item">
                                                    <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                    <div class="menu__item-submenu-offset"></div>
                                                    <a class="menu__item-link" href="">
                                                        Order History
                                                    </a>
                                                </li>
                                                <li class="menu__item">
                                                    <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                    <div class="menu__item-submenu-offset"></div>
                                                    <a class="menu__item-link" href="">
                                                        Addresses
                                                    </a>
                                                </li>
                                                <li class="menu__item">
                                                    <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                    <div class="menu__item-submenu-offset"></div>
                                                    <a class="menu__item-link" href="">
                                                        Password
                                                    </a>
                                                </li>
                                                <li class="menu__item">
                                                    <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                    <div class="menu__item-submenu-offset"></div>
                                                    <a class="menu__item-link" href="">
                                                        Logout
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- .menu / end -->
                                    </div>
                                </div>
                            </div>



                          
                            <div class="topbar__item">
                                <div class="topbar-dropdown">
                                    <button class="topbar-dropdown__btn" type="button" id="">
                                        Language: <span class="topbar__item-value">
                                               @if(get_setting('show_language_switcher') == 'on')
                   
                        @php
                            if(Session::has('locale')){
                                $locale = Session::get('locale', Config::get('app.locale'));
                            }
                            else{
                                $locale = 'en';
                            }
                        @endphp

    {{ \App\Language::where('code', $locale)->first()->name }}
                       
                                        </span>
                                        <svg width="7px" height="5px">
                                            <use xlink:href="frontend/images/sprite.svg#arrow-rounded-down-7x5"></use>
                                        </svg>
                                    </button>
                                    <div class="topbar-dropdown__body">
                                        <!-- .menu -->
                                        <div class="menu menu--layout--topbar  menu--with-icons ">
                                            <div class="menu__submenus-container"></div>
                                            <ul class="menu__list">
                                                                          @foreach (\App\Language::all() as $key => $language)
                     


                                                <li class="menu__item " id="languages1">
                                                    <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                    <div class="menu__item-submenu-offset" ></div>
                                                    <a class="menu__item-link" href="javascript:void(0)" data-flag="{{ $language->code }}">
                                                        <div class="menu__item-icon"><img srcset="frontend/images/languages/language-1.png 1x, images/languages/language-1@2x.png 2x" src="images/languages/language-1.png" alt=""></div>
                                                        {{ $language->name }}
                                                    </a>
                                                </li>      
                            @endforeach
                                                                             @endif      
 
                                            </ul>
                                        </div>
                                        <!-- .menu / end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .topbar / end -->
                <div class="site-header__middle container">
                    <div class="site-header__logo">
                        <a href="index.html">
                            <!-- logo -->

                            <img width="188px" src="{{static_asset('frontend/images/zk-logo/logo H_350mm(sticker)ss.png')}}" >
      
                            <!-- logo / end -->
                        </a>
                    </div>
                    <div class="site-header__search">
                        <div class="search search--location--header ">
                            <div class="search__body">
                                    <form class="search__form" action="{{ route('search') }}" method="GET" >
                                        <input type="text" class="search__input"name="q" placeholder="{{translate('I am shopping for...')}}" autocomplete="off"
                                         id="searchd"  >
                                        <button class="search__button search__button--type--submit" type="submit">
                                            <svg width="20px" height="20px">
                                                <use xlink:href="{{static_asset('frontend/images/sprite.svg#search-20')}}"></use>
                                            </svg>
                                        </button>
                                        <button class="search__button search__button--type--close" type="button">
                                            <svg width="20px" height="20px">
                                                <use xlink:href="{{static_asset('frontend/images/sprite.svg#cross-20')}}"></use>
                                            </svg>
                                        </button>
                                        <div class="search__border"></div>
                                    </form>

                                            <div class="typed-search-box stop-propagation document-click-d-none d-none bg-white rounded shadow-lg position-absolute left-0 top-100 w-100" style="min-height: 200px;color: #000;">
                            <div class="search-preloader absolute-top-center">
                                <div class="dot-loader"><div></div><div></div><div></div></div>
                            </div>
                            <div class="search-nothing d-none p-3 text-center fs-16">

                            </div>
                            <div class="searchcontent" class="text-left">

                            </div>
                        </div>
                                <div class="search__suggestions suggestions suggestions--location--header"></div>
                            </div>
                        </div>
                    </div>
                    <div class="site-header__phone">
                        <div class="site-header__phone-title">Customer Service</div>
                        <div class="site-header__phone-number">199050 </div>
                    </div>
                </div>


                     
                <div class="site-header__nav-panel">
                    <!-- data-sticky-mode - one of [pullToShow, alwaysOnTop] -->
                    <div class="nav-panel nav-panel--sticky" data-sticky-mode="pullToShow">
                        <div class="nav-panel__container container">
                            <div class="nav-panel__row">
                                <div class="nav-panel__departments">
                                    <!-- .departments -->
                                    <div class="departments " data-departments-fixed-by="">
                                        <div class="departments__body">
                                            <div class="departments__links-wrapper">
                                                <div class="departments__submenus-container"></div>
                                                <ul class="departments__links">
                                                    <li class="departments__item">
                                                        <a class="departments__item-link" href="">
                                                            Power Tools
                                                            <svg class="departments__item-arrow" width="6px" height="9px">
                                                                <use xlink:href="images/sprite.svg#arrow-rounded-right-6x9"></use>
                                                            </svg>
                                                        </a>
                                                        <div class="departments__submenu departments__submenu--type--megamenu departments__submenu--size--xl">
                                                            <!-- .megamenu -->
                                                            <div class="megamenu  megamenu--departments ">
                                                                <div class="megamenu__body" style="background-image: url('images/megamenu/megamenu-1.jpg');">
                                                                    <div class="row">
                                                                        <div class="col-3">
                                                                            <ul class="megamenu__links megamenu__links--level--0">
                                                                                <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                                    <a href="">Power Tools</a>
                                                                                    <ul class="megamenu__links megamenu__links--level--1">
                                                                                        <li class="megamenu__item"><a href="">Engravers</a></li>
                                                                                        <li class="megamenu__item"><a href="">Drills</a></li>
                                                                                        <li class="megamenu__item"><a href="">Wrenches</a></li>
                                                                                        <li class="megamenu__item"><a href="">Plumbing</a></li>
                                                                                        <li class="megamenu__item"><a href="">Wall Chaser</a></li>
                                                                                        <li class="megamenu__item"><a href="">Pneumatic Tools</a></li>
                                                                                        <li class="megamenu__item"><a href="">Milling Cutters</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li class="megamenu__item ">
                                                                                    <a href="">Workbenches</a>
                                                                                </li>
                                                                                <li class="megamenu__item ">
                                                                                    <a href="">Presses</a>
                                                                                </li>
                                                                                <li class="megamenu__item ">
                                                                                    <a href="">Spray Guns</a>
                                                                                </li>
                                                                                <li class="megamenu__item ">
                                                                                    <a href="">Riveters</a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-3">
                                                                            <ul class="megamenu__links megamenu__links--level--0">
                                                                                <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                                    <a href="">Hand Tools</a>
                                                                                    <ul class="megamenu__links megamenu__links--level--1">
                                                                                        <li class="megamenu__item"><a href="">Screwdrivers</a></li>
                                                                                        <li class="megamenu__item"><a href="">Handsaws</a></li>
                                                                                        <li class="megamenu__item"><a href="">Knives</a></li>
                                                                                        <li class="megamenu__item"><a href="">Axes</a></li>
                                                                                        <li class="megamenu__item"><a href="">Multitools</a></li>
                                                                                        <li class="megamenu__item"><a href="">Paint Tools</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                                    <a href="">Garden Equipment</a>
                                                                                    <ul class="megamenu__links megamenu__links--level--1">
                                                                                        <li class="megamenu__item"><a href="">Motor Pumps</a></li>
                                                                                        <li class="megamenu__item"><a href="">Chainsaws</a></li>
                                                                                        <li class="megamenu__item"><a href="">Electric Saws</a></li>
                                                                                        <li class="megamenu__item"><a href="">Brush Cutters</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-3">
                                                                            <ul class="megamenu__links megamenu__links--level--0">
                                                                                <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                                    <a href="">Machine Tools</a>
                                                                                    <ul class="megamenu__links megamenu__links--level--1">
                                                                                        <li class="megamenu__item"><a href="">Thread Cutting</a></li>
                                                                                        <li class="megamenu__item"><a href="">Chip Blowers</a></li>
                                                                                        <li class="megamenu__item"><a href="">Sharpening Machines</a></li>
                                                                                        <li class="megamenu__item"><a href="">Pipe Cutters</a></li>
                                                                                        <li class="megamenu__item"><a href="">Slotting machines</a></li>
                                                                                        <li class="megamenu__item"><a href="">Lathes</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-3">
                                                                            <ul class="megamenu__links megamenu__links--level--0">
                                                                                <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                                    <a href="">Instruments</a>
                                                                                    <ul class="megamenu__links megamenu__links--level--1">
                                                                                        <li class="megamenu__item"><a href="">Welding Equipment</a></li>
                                                                                        <li class="megamenu__item"><a href="">Power Tools</a></li>
                                                                                        <li class="megamenu__item"><a href="">Hand Tools</a></li>
                                                                                        <li class="megamenu__item"><a href="">Measuring Tool</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- .megamenu / end -->
                                                        </div>
                                                    </li>
                                                    <li class="departments__item">
                                                        <a class="departments__item-link" href="">
                                                            Hand Tools
                                                            <svg class="departments__item-arrow" width="6px" height="9px">
                                                                <use xlink:href="images/sprite.svg#arrow-rounded-right-6x9"></use>
                                                            </svg>
                                                        </a>
                                                        <div class="departments__submenu departments__submenu--type--megamenu departments__submenu--size--lg">
                                                            <!-- .megamenu -->
                                                            <div class="megamenu  megamenu--departments ">
                                                                <div class="megamenu__body" style="background-image: url('images/megamenu/megamenu-2.jpg');">
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <ul class="megamenu__links megamenu__links--level--0">
                                                                                <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                                    <a href="">Hand Tools</a>
                                                                                    <ul class="megamenu__links megamenu__links--level--1">
                                                                                        <li class="megamenu__item"><a href="">Screwdrivers</a></li>
                                                                                        <li class="megamenu__item"><a href="">Handsaws</a></li>
                                                                                        <li class="megamenu__item"><a href="">Knives</a></li>
                                                                                        <li class="megamenu__item"><a href="">Axes</a></li>
                                                                                        <li class="megamenu__item"><a href="">Multitools</a></li>
                                                                                        <li class="megamenu__item"><a href="">Paint Tools</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                                    <a href="">Garden Equipment</a>
                                                                                    <ul class="megamenu__links megamenu__links--level--1">
                                                                                        <li class="megamenu__item"><a href="">Motor Pumps</a></li>
                                                                                        <li class="megamenu__item"><a href="">Chainsaws</a></li>
                                                                                        <li class="megamenu__item"><a href="">Electric Saws</a></li>
                                                                                        <li class="megamenu__item"><a href="">Brush Cutters</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <ul class="megamenu__links megamenu__links--level--0">
                                                                                <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                                    <a href="">Machine Tools</a>
                                                                                    <ul class="megamenu__links megamenu__links--level--1">
                                                                                        <li class="megamenu__item"><a href="">Thread Cutting</a></li>
                                                                                        <li class="megamenu__item"><a href="">Chip Blowers</a></li>
                                                                                        <li class="megamenu__item"><a href="">Sharpening Machines</a></li>
                                                                                        <li class="megamenu__item"><a href="">Pipe Cutters</a></li>
                                                                                        <li class="megamenu__item"><a href="">Slotting machines</a></li>
                                                                                        <li class="megamenu__item"><a href="">Lathes</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <ul class="megamenu__links megamenu__links--level--0">
                                                                                <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                                    <a href="">Instruments</a>
                                                                                    <ul class="megamenu__links megamenu__links--level--1">
                                                                                        <li class="megamenu__item"><a href="">Welding Equipment</a></li>
                                                                                        <li class="megamenu__item"><a href="">Power Tools</a></li>
                                                                                        <li class="megamenu__item"><a href="">Hand Tools</a></li>
                                                                                        <li class="megamenu__item"><a href="">Measuring Tool</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- .megamenu / end -->
                                                        </div>
                                                    </li>
                                                    <li class="departments__item">
                                                        <a class="departments__item-link" href="">
                                                            Machine Tools
                                                            <svg class="departments__item-arrow" width="6px" height="9px">
                                                                <use xlink:href="images/sprite.svg#arrow-rounded-right-6x9"></use>
                                                            </svg>
                                                        </a>
                                                        <div class="departments__submenu departments__submenu--type--megamenu departments__submenu--size--nl">
                                                            <!-- .megamenu -->
                                                            <div class="megamenu  megamenu--departments ">
                                                                <div class="megamenu__body" style="background-image: url('images/megamenu/megamenu-3.jpg');">
                                                                    <div class="row">
                                                                        <div class="col-6">
                                                                            <ul class="megamenu__links megamenu__links--level--0">
                                                                                <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                                    <a href="">Hand Tools</a>
                                                                                    <ul class="megamenu__links megamenu__links--level--1">
                                                                                        <li class="megamenu__item"><a href="">Screwdrivers</a></li>
                                                                                        <li class="megamenu__item"><a href="">Handsaws</a></li>
                                                                                        <li class="megamenu__item"><a href="">Knives</a></li>
                                                                                        <li class="megamenu__item"><a href="">Axes</a></li>
                                                                                        <li class="megamenu__item"><a href="">Multitools</a></li>
                                                                                        <li class="megamenu__item"><a href="">Paint Tools</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                                    <a href="">Garden Equipment</a>
                                                                                    <ul class="megamenu__links megamenu__links--level--1">
                                                                                        <li class="megamenu__item"><a href="">Motor Pumps</a></li>
                                                                                        <li class="megamenu__item"><a href="">Chainsaws</a></li>
                                                                                        <li class="megamenu__item"><a href="">Electric Saws</a></li>
                                                                                        <li class="megamenu__item"><a href="">Brush Cutters</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <ul class="megamenu__links megamenu__links--level--0">
                                                                                <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                                    <a href="">Instruments</a>
                                                                                    <ul class="megamenu__links megamenu__links--level--1">
                                                                                        <li class="megamenu__item"><a href="">Welding Equipment</a></li>
                                                                                        <li class="megamenu__item"><a href="">Power Tools</a></li>
                                                                                        <li class="megamenu__item"><a href="">Hand Tools</a></li>
                                                                                        <li class="megamenu__item"><a href="">Measuring Tool</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- .megamenu / end -->
                                                        </div>
                                                    </li>
                                                    <li class="departments__item">
                                                        <a class="departments__item-link" href="">
                                                            Building Supplies
                                                            <svg class="departments__item-arrow" width="6px" height="9px">
                                                                <use xlink:href="images/sprite.svg#arrow-rounded-right-6x9"></use>
                                                            </svg>
                                                        </a>
                                                        <div class="departments__submenu departments__submenu--type--megamenu departments__submenu--size--sm">
                                                            <!-- .megamenu -->
                                                            <div class="megamenu  megamenu--departments ">
                                                                <div class="megamenu__body">
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <ul class="megamenu__links megamenu__links--level--0">
                                                                                <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                                    <a href="">Hand Tools</a>
                                                                                    <ul class="megamenu__links megamenu__links--level--1">
                                                                                        <li class="megamenu__item"><a href="">Screwdrivers</a></li>
                                                                                        <li class="megamenu__item"><a href="">Handsaws</a></li>
                                                                                        <li class="megamenu__item"><a href="">Knives</a></li>
                                                                                        <li class="megamenu__item"><a href="">Axes</a></li>
                                                                                        <li class="megamenu__item"><a href="">Multitools</a></li>
                                                                                        <li class="megamenu__item"><a href="">Paint Tools</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li class="megamenu__item  megamenu__item--with-submenu ">
                                                                                    <a href="">Garden Equipment</a>
                                                                                    <ul class="megamenu__links megamenu__links--level--1">
                                                                                        <li class="megamenu__item"><a href="">Motor Pumps</a></li>
                                                                                        <li class="megamenu__item"><a href="">Chainsaws</a></li>
                                                                                        <li class="megamenu__item"><a href="">Electric Saws</a></li>
                                                                                        <li class="megamenu__item"><a href="">Brush Cutters</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- .megamenu / end -->
                                                        </div>
                                                    </li>
                                                    <li class="departments__item">
                                                        <a class="departments__item-link" href="">
                                                            Electrical
                                                            <svg class="departments__item-arrow" width="6px" height="9px">
                                                                <use xlink:href="images/sprite.svg#arrow-rounded-right-6x9"></use>
                                                            </svg>
                                                        </a>
                                                        <div class="departments__submenu departments__submenu--type--menu">
                                                            <!-- .menu -->
                                                            <div class="menu menu--layout--classic ">
                                                                <div class="menu__submenus-container"></div>
                                                                <ul class="menu__list">
                                                                    <li class="menu__item">
                                                                        <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                                        <div class="menu__item-submenu-offset"></div>
                                                                        <a class="menu__item-link" href="">
                                                                            Soldering Equipment
                                                                            <svg class="menu__item-arrow" width="6px" height="9px">
                                                                                <use xlink:href="images/sprite.svg#arrow-rounded-right-6x9"></use>
                                                                            </svg>
                                                                        </a>
                                                                        <div class="menu__submenu">
                                                                            <!-- .menu -->
                                                                            <div class="menu menu--layout--classic ">
                                                                                <div class="menu__submenus-container"></div>
                                                                                <ul class="menu__list">
                                                                                    <li class="menu__item">
                                                                                        <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                                                        <div class="menu__item-submenu-offset"></div>
                                                                                        <a class="menu__item-link" href="">
                                                                                            Soldering Station
                                                                                        </a>
                                                                                    </li>
                                                                                    <li class="menu__item">
                                                                                        <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                                                        <div class="menu__item-submenu-offset"></div>
                                                                                        <a class="menu__item-link" href="">
                                                                                            Soldering Dryers
                                                                                        </a>
                                                                                    </li>
                                                                                    <li class="menu__item">
                                                                                        <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                                                        <div class="menu__item-submenu-offset"></div>
                                                                                        <a class="menu__item-link" href="">
                                                                                            Gas Soldering Iron
                                                                                        </a>
                                                                                    </li>
                                                                                    <li class="menu__item">
                                                                                        <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                                                        <div class="menu__item-submenu-offset"></div>
                                                                                        <a class="menu__item-link" href="">
                                                                                            Electric Soldering Iron
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <!-- .menu / end -->
                                                                        </div>
                                                                    </li>
                                                                    <li class="menu__item">
                                                                        <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                                        <div class="menu__item-submenu-offset"></div>
                                                                        <a class="menu__item-link" href="">
                                                                            Light Bulbs
                                                                        </a>
                                                                    </li>
                                                                    <li class="menu__item">
                                                                        <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                                        <div class="menu__item-submenu-offset"></div>
                                                                        <a class="menu__item-link" href="">
                                                                            Batteries
                                                                        </a>
                                                                    </li>
                                                                    <li class="menu__item">
                                                                        <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                                        <div class="menu__item-submenu-offset"></div>
                                                                        <a class="menu__item-link" href="">
                                                                            Light Fixtures
                                                                        </a>
                                                                    </li>
                                                                    <li class="menu__item">
                                                                        <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                                        <div class="menu__item-submenu-offset"></div>
                                                                        <a class="menu__item-link" href="">
                                                                            Warm Floor
                                                                        </a>
                                                                    </li>
                                                                    <li class="menu__item">
                                                                        <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                                        <div class="menu__item-submenu-offset"></div>
                                                                        <a class="menu__item-link" href="">
                                                                            Generators
                                                                        </a>
                                                                    </li>
                                                                    <li class="menu__item">
                                                                        <!-- This is a synthetic element that allows to adjust the vertical offset of the submenu using CSS. -->
                                                                        <div class="menu__item-submenu-offset"></div>
                                                                        <a class="menu__item-link" href="">
                                                                            UPS
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <!-- .menu / end -->
                                                        </div>
                                                    </li>
                                                    <li class="departments__item">
                                                        <a class="departments__item-link" href="">
                                                            Power Machinery
                                                        </a>
                                                    </li>
                                                    <li class="departments__item">
                                                        <a class="departments__item-link" href="">
                                                            Measurement
                                                        </a>
                                                    </li>
                                                    <li class="departments__item">
                                                        <a class="departments__item-link" href="">
                                                            Clothes & PPE
                                                        </a>
                                                    </li>
                                                    <li class="departments__item">
                                                        <a class="departments__item-link" href="">
                                                            Plumbing
                                                        </a>
                                                    </li>
                                                    <li class="departments__item">
                                                        <a class="departments__item-link" href="">
                                                            Storage & Organization
                                                        </a>
                                                    </li>
                                                    <li class="departments__item">
                                                        <a class="departments__item-link" href="">
                                                            Welding & Soldering
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <button class="departments__button" id="cllick">
                                            <svg class="departments__button-icon" width="18px" height="14px">
                                                <use xlink:href="images/sprite.svg#menu-18x14"></use>
                                            </svg>
                                            Products
                                                <svg class="departments__button-arrow" width="9px" height="6px">
                                                <use xlink:href="images/sprite.svg#arrow-rounded-down-9x6"></use>
                                            </svg>
                                        </button>
                                    </div>
                                    <!-- .departments / end -->
                                </div>
                                <!-- .nav-links -->
                                <div class="nav-panel__nav-links nav-links">
                                    <ul class="nav-links__list">

                                        <li class="nav-links__item ">
                                            <a class="nav-links__item-link" href="">
                                                <div class="nav-links__item-body">
                                                    Solutions
                                                </div>
                                            </a>
                                        </li>


                                        <li class="nav-links__item ">
                                            <a class="nav-links__item-link" href="">
                                                <div class="nav-links__item-body">
                                                    Support
                                                </div>
                                            </a>
                                        </li>

                                        
                                        <li class="nav-links__item ">
                                            <a class="nav-links__item-link" href="">
                                                <div class="nav-links__item-body">
                                                    News
                                                </div>
                                            </a>
                                        </li>


                                        <li class="nav-links__item ">
                                            <a class="nav-links__item-link" href="">
                                                <div class="nav-links__item-body">
                                                    Company
                                                </div>
                                            </a>
                                        </li>

                                        


                                        <li class="nav-links__item ">
                                            <a class="nav-links__item-link" href="">
                                                <div class="nav-links__item-body">
                                                    Our Clients
                                                </div>
                                            </a>
                                        </li>
                                        
            
                                        

                                        <li class="nav-links__item ">
                                            <a class="nav-links__item-link" href="">
                                                <div class="nav-links__item-body">
                                                Contact Us
                                                </div>
                                            </a>
                                        </li>





                                    
                                    </ul>
                                </div>
                                <!-- .nav-links / end -->
                                <div class="nav-panel__indicators">
                                    <div class="indicator">
                                        <a href="wishlist.html" class="indicator__button">
                                            <span class="indicator__area">
                                                <svg width="20px" height="20px">
                                                    <use xlink:href="{{static_asset('frontend/images/sprite.svg#heart-20')}}"></use>
                                                </svg>
                                                <span class="indicator__value">0</span>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="indicator indicator--trigger--click" id="cart_items">
                                                              @include('frontend.partials.newcart')
                                            <!-- .dropcart -->
                            <!--                 <div class="dropcart dropcart--style--dropdown">
                                                <div class="dropcart__body">
                                                    <div class="dropcart__products-list">
                                                        <div class="dropcart__product">
                                                            <div class="product-image dropcart__product-image">
                                                                <a href="product.html" class="product-image__body">
                                                                    <img class="product-image__img" src="{{static_asset('frontend/images/products/product-1.jpg')}}" alt="">
                                                                </a>
                                                            </div>
                                                            <div class="dropcart__product-info">
                                                                <div class="dropcart__product-name"><a href="product.html">Electric Planer Brandix KL370090G 300 Watts</a></div>
                                                                <ul class="dropcart__product-options">
                                                                    <li>Color: Yellow</li>
                                                                    <li>Material: Aluminium</li>
                                                                </ul>
                                                                <div class="dropcart__product-meta">
                                                                    <span class="dropcart__product-quantity">2</span> ×
                                                                    <span class="dropcart__product-price">$699.00</span>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="dropcart__product-remove btn btn-light btn-sm btn-svg-icon">
                                                                <svg width="10px" height="10px">
                                                                    <use xlink:href="{{static_asset('frontend/images/sprite.svg#cross-10')}}'"></use>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                                                               <div class="dropcart__totals">
                                                        <table>
                                                            <tr>
                                                                <th>Subtotal</th>
                                                                <td>$5,877.00</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Shipping</th>
                                                                <td>$25.00</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Tax</th>
                                                                <td>$0.00</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Total</th>
                                                                <td>$5,902.00</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="dropcart__buttons">
                                                        <a class="btn btn-secondary" href="cart.html">View Cart</a>
                                                        <a class="btn btn-primary" href="checkout.html">Checkout</a>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- .dropcart / end -->
                                     <!--    </div>
                                    </div>
                                    <div class="indicator indicator--trigger--click">
                                        <a href="account-login.html" class="indicator__button">
                                            <span class="indicator__area">
                                                <svg width="20px" height="20px">
                                                    <use xlink:href="images/sprite.svg#person-20"></use>
                                                </svg>
                                            </span>
                                        </a> -->
                                <!--         <div class="indicator__dropdown">
                                            <div class="account-menu">
                                                <form class="account-menu__form">
                                                    <div class="account-menu__form-title">Log In to Your Account</div>
                                                    <div class="form-group">
                                                        <label for="header-signin-email" class="sr-only">Email address</label>
                                                        <input id="header-signin-email" type="email" class="form-control form-control-sm" placeholder="Email address">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="header-signin-password" class="sr-only">Password</label>
                                                        <div class="account-menu__form-forgot">
                                                            <input id="header-signin-password" type="password" class="form-control form-control-sm" placeholder="Password">
                                                            <a href="" class="account-menu__form-forgot-link">Forgot?</a>
                                                        </div>
                                                    </div>
                                                    <div class="form-group account-menu__form-button">
                                                        <button type="submit" class="btn btn-primary btn-sm">Login</button>
                                                    </div>
                                                    <div class="account-menu__form-link"><a href="account-login.html">Create An Account</a></div>
                                                </form>
                                                <div class="account-menu__divider"></div>
                                                <a href="account-dashboard.html" class="account-menu__user">
                                                    <div class="account-menu__user-avatar">
                                                        <img src="images/avatars/avatar-3.jpg" alt="">
                                                    </div>
                                                    <div class="account-menu__user-info">
                                                        <div class="account-menu__user-name">Helena Garcia</div>
                                                        <div class="account-menu__user-email">zktecho@example.com</div>
                                                    </div>
                                                </a>
                                                <div class="account-menu__divider"></div>
                                                <ul class="account-menu__links">
                                                    <li><a href="account-profile.html">Edit Profile</a></li>
                                                    <li><a href="account-orders.html">Order History</a></li>
                                                    <li><a href="account-addresses.html">Addresses</a></li>
                                                    <li><a href="account-password.html">Password</a></li>
                                                </ul>
                                                <div class="account-menu__divider"></div>
                                                <ul class="account-menu__links">
                                                    <li><a href="account-login.html">Logout</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </header>

   